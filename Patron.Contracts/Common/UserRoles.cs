﻿namespace Patron.Contracts.Common;

public record UserRoles(
    string Id,
    string Name);