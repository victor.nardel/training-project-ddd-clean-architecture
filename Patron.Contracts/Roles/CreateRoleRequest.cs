﻿namespace Patron.Contracts.Roles;

public record CreateRoleRequest(
    string Name);