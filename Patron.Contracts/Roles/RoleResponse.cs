﻿namespace Patron.Contracts.Roles;

public record RoleResponse(
    string Id,
    string Name);