﻿namespace Patron.Contracts.Reservations;

public record ReservationResponse(
    string Id,
    DateTime DateD,
    DateTime DateF,
    float Amount,
    bool IsPaid,
    string ClientId,
    string RoomId,
    int PeopleNumber,
    DateTime CreatedAt,
    DateTime UpdatedAt);