﻿namespace Patron.Contracts.Reservations;

public record CreateReservationRequest(
    DateTime DateD,
    DateTime DateF,
    float Amount,
    bool IsPaid,
    string ClientId,
    string RoomId,
    int PeopleNumber);