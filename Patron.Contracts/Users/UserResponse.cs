﻿using Patron.Contracts.Common;

namespace Patron.Contracts.Users;

public record UserResponse(
    string Id,
    string UserId,
    string FirstName,
    string LastName,
    string Email,
    List<UserRoles> Roles);