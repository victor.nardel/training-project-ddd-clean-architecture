﻿using Patron.Contracts.Common;

namespace Patron.Contracts.Users;

public record UpdateUserRequest(
    string Email,
    string Password,
    string FirstName,
    string LastName,
    List<UserRoles> Roles);