﻿using Patron.Contracts.Common;

namespace Patron.Contracts.Authentication;

public record RegisterRequest(
    string Email,
    string Password,
    string FirstName,
    string LastName,
    List<string> RoleIds);