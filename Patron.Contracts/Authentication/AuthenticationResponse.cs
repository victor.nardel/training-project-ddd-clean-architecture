﻿using Patron.Contracts.Common;

namespace Patron.Contracts.Authentication;

public record AuthenticationResponse(
    string FirstName,
    string LastName,
    string Email,
    string Token);