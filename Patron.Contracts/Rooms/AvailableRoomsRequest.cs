﻿namespace Patron.Contracts.Rooms;

public record AvailableRoomsRequest(
    DateTime DateD,
    DateTime DateF);