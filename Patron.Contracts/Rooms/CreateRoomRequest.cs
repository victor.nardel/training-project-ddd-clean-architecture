﻿namespace Patron.Contracts.Rooms;

public record CreateRoomRequest(
    int MaxPeople,
    string Type,
    float Price,
    DateTime CleanedDate,
    bool IsCleaned,
    string State);