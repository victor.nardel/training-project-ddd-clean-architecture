﻿namespace Patron.Contracts.Rooms;

public record RoomResponse(
    string Id,
    int MaxPeople,
    string Type,
    float Price,
    DateTime CleanedDate,
    bool IsCleaned,
    string State,
    DateTime CreatedAt,
    DateTime UpdatedAt);