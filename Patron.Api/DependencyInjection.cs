﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Patron.Api.Common.Errors;
using Patron.Api.Common.Mapping;
using Patron.Api.Helpers.AuthorizationPoliciesHelper;

namespace Patron.Api;
public static class DependencyInjection
{
    public static IServiceCollection AddPresentation(this IServiceCollection services)
    {
        services.AddControllers();

        services
            .AddSingleton<ProblemDetailsFactory, PatronProblemDetailsFactory>()
            .AddMappings();

        services.AddAuthorizationPolicies();

        return services;
    }
}