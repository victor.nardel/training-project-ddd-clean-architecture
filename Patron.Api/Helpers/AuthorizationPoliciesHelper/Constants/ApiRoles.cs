﻿using System.Reflection;

namespace Patron.Api.Helpers.AuthorizationPoliciesHelper.Constants;

public static class ApiRoles
{
    public static readonly string Admin = "admin";
    public static readonly string User = "user";

    private static List<string> ListAllRoles() =>
        typeof(ApiRoles).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
            .Where(field => field.FieldType == typeof(string))
            .Select(field => field.GetValue(null))
            .Cast<string>()
            .ToList();

    public static IReadOnlyList<string> Roles { get; } = ListAllRoles();
}