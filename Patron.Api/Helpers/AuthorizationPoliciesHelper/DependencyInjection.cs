﻿using Patron.Api.Helpers.AuthorizationPoliciesHelper.Constants;

namespace Patron.Api.Helpers.AuthorizationPoliciesHelper;

public static class DependencyInjection
{
    public static IServiceCollection AddAuthorizationPolicies(this IServiceCollection services) =>
        services.AddAuthorization(options =>
        {
            foreach (var role in ApiRoles.Roles)
            {
                options.AddPolicy(role, policy => policy.RequireRole(role));
            }
        });
}