﻿using MapsterMapper;

using MediatR;

using Microsoft.AspNetCore.Mvc;

using Patron.Application.Users.Commands.UpdateUser;
using Patron.Contracts.Users;

namespace Patron.Api.Controllers;

[Route("v1/[controller]")]
public class UsersController(ISender mediator, IMapper mapper) : ApiController
{
    [HttpPut]
    public async Task<IActionResult> Update(UpdateUserRequest request)
    {
        var command = mapper.Map<UpdateUserCommand>(request);

        return (await mediator.Send(command))
            .Match(
                user => Ok(mapper.Map<UserResponse>(user)),
                Problem);
    }
}