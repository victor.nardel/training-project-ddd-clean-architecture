﻿using MapsterMapper;

using MediatR;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Patron.Application.Roles.Commands.CreateRole;
using Patron.Application.Roles.Queries.AllRoles;
using Patron.Contracts.Roles;

namespace Patron.Api.Controllers;

[Route("v1/[controller]")]
[Authorize(Policy = "admin")]
public class RolesController(ISender mediator, IMapper mapper) : ApiController
{
    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var command = mapper.Map<AllRolesQuery>(new AllRolesQuery());

        return (await mediator.Send(command))
            .Match(
                rooms => Ok(rooms.Select(mapper.Map<RoleResponse>)),
                Problem);
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody] CreateRoleRequest request)
    {
        var command = mapper.Map<CreateRoleCommand>(request);

        return (await mediator.Send(command))
            .Match(
                menu => Ok(mapper.Map<RoleResponse>(menu)),
                Problem);
    }
}