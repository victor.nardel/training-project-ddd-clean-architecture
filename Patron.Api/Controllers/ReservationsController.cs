﻿using MapsterMapper;

using MediatR;

using Microsoft.AspNetCore.Mvc;

using Patron.Application.Reservations.Commands.CreateReservation;
using Patron.Application.Reservations.Commands.DeleteReservation;
using Patron.Application.Reservations.Queries.AllReservations;
using Patron.Contracts.Reservations;

namespace Patron.Api.Controllers;

[Route("v1/[controller]")]
public class ReservationsController(ISender mediator, IMapper mapper) : ApiController
{
    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var command = mapper.Map<AllReservationsQuery>(new AllReservationsQuery());

        return (await mediator.Send(command))
            .Match(
                rooms => Ok(rooms.Select(mapper.Map<ReservationResponse>)),
                Problem);
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody] CreateReservationRequest request)
    {
        var command = mapper.Map<CreateReservationCommand>(request);

        return (await mediator.Send(command))
            .Match(
                reservation => Ok(mapper.Map<ReservationResponse>(reservation)),
                Problem);
    }

    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> Delete(string id)
    {
        var command = mapper.Map<DeleteReservationCommand>(id);

        return (await mediator.Send(command))
            .Match(
                menu => Ok(mapper.Map<ReservationResponse>(menu)),
                Problem);
    }
}