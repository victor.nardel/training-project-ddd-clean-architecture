﻿using MapsterMapper;

using MediatR;

using Microsoft.AspNetCore.Mvc;

using Patron.Application.Rooms.Commands.CreateRoom;
using Patron.Application.Rooms.Queries.AllRooms;
using Patron.Contracts.Rooms;

namespace Patron.Api.Controllers;

[Route("v1/[controller]")]
public class RoomsController(ISender mediator, IMapper mapper) : ApiController
{
    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var query = mapper.Map<AllRoomsQuery>(new AllRoomsQuery());

        return (await mediator.Send(query))
            .Match(
                rooms => Ok(rooms.Select(mapper.Map<RoomResponse>)),
                Problem);
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody] CreateRoomRequest request)
    {
        var command = mapper.Map<CreateRoomCommand>(request);

        return (await mediator.Send(command))
            .Match(
                menu => Ok(mapper.Map<RoomResponse>(menu)),
                Problem);
    }
}