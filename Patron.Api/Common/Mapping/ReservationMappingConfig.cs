﻿using Mapster;

using Patron.Application.Reservations.Commands.CreateReservation;
using Patron.Application.Reservations.Commands.DeleteReservation;
using Patron.Contracts.Reservations;
using Patron.Domain.ReservationAggregate;
using Patron.Domain.ReservationAggregate.ValueObjects;

namespace Patron.Api.Common.Mapping;

public class ReservationMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<CreateReservationRequest, CreateReservationCommand>();

        config.NewConfig<string, DeleteReservationCommand>()
            .MapWith(src => new DeleteReservationCommand(src));

        config.NewConfig<Reservation, ReservationResponse>();
    }
}