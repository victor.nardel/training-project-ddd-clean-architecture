﻿using Mapster;

using Patron.Application.Roles.Commands.CreateRole;
using Patron.Contracts.Roles;
using Patron.Contracts.Rooms;
using Patron.Domain.RoleAggregate;

namespace Patron.Api.Common.Mapping;

public class RoleMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<CreateRoleRequest, CreateRoleCommand>();

        config.NewConfig<Role, RoomResponse>();
    }
}