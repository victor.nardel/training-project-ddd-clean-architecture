﻿using Mapster;

using Patron.Application.Authentication.Authentication.Commands.Register;
using Patron.Application.Authentication.Authentication.Common;
using Patron.Application.Authentication.Authentication.Queries.Login;
using Patron.Contracts.Authentication;

namespace Patron.Api.Common.Mapping;

public class AuthenticationMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<RegisterRequest, RegisterCommand>();

        config.NewConfig<LoginRequest, LoginQuery>();

        config.NewConfig<AuthenticationResult, AuthenticationResponse>()
            .Map(dest => dest, src => src.User);
    }
}