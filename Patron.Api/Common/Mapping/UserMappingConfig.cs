﻿using Mapster;

using Patron.Application.Users.Commands.UpdateUser;
using Patron.Contracts.Users;
using Patron.Domain.UserAggregate;

namespace Patron.Api.Common.Mapping;

public class UserMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<UpdateUserRequest, UpdateUserCommand>();

        config.NewConfig<User, UserResponse>();
    }
}