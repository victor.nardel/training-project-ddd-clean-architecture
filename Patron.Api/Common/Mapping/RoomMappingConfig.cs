﻿using Mapster;

using Patron.Application.Rooms.Commands.CreateRoom;
using Patron.Contracts.Rooms;
using Patron.Domain.RoomAggregate;

namespace Patron.Api.Common.Mapping;

public class RoomMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<CreateRoomRequest, CreateRoomCommand>();

        config.NewConfig<Room, RoomResponse>();
    }
}