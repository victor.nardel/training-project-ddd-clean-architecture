﻿using Patron.Domain.Common.Models;
using Patron.Domain.ReservationAggregate;
using Patron.Domain.RoomAggregate.ValueObjects;

namespace Patron.Domain.RoomAggregate;

public sealed class Room : AggregateRoot<RoomId, Guid>
{
    public int MaxPeople { get; private set; }
    public string Type { get; private set; }
    public float Price { get; private set; }
    public DateTime CleanedDate { get; private set; }
    public bool IsCleaned { get; private set; }
    public string State { get; private set; }
    public DateTime CreatedAt { get; private set; }
    public DateTime UpdatedAt { get; private set; }

    private Room(
        RoomId roomId,
        int maxPeople,
        string type,
        float price,
        DateTime cleanedDate,
        bool isCleaned,
        string state,
        DateTime? createdAt = null)
        : base(roomId)
    {
        MaxPeople = maxPeople;
        Type = type;
        Price = price;
        CleanedDate = cleanedDate;
        IsCleaned = isCleaned;
        State = state;
        CreatedAt = createdAt ?? CreatedAt;
        UpdatedAt = DateTime.Now;
    }

    public static Room Create(
        int maxPeople,
        string type,
        float price,
        DateTime cleanedDate,
        bool isCleaned,
        string state)
    {
        return new Room(
            RoomId.CreateUnique(),
            maxPeople,
            type,
            price,
            cleanedDate,
            isCleaned,
            state,
            DateTime.Now);
    }

#pragma warning disable CS8618
    private Room()
    {
    }
#pragma warning restore CS8618
}