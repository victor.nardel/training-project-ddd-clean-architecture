﻿using Patron.Domain.Common.Models.Identities;

namespace Patron.Domain.RoomAggregate.ValueObjects;

public sealed class RoomId : AggregateRootId<Guid>
{
    private RoomId(Guid value) : base(value)
    {
    }

    public static RoomId Create(Guid roomId) =>
        new(roomId);

    public static RoomId CreateUnique() =>
        new(Guid.NewGuid());
}