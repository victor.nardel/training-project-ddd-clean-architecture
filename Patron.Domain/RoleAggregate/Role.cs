﻿using Patron.Domain.Common.Models;
using Patron.Domain.RoleAggregate.ValueObjects;
using Patron.Domain.UserAggregate;

namespace Patron.Domain.RoleAggregate;

public sealed class Role : AggregateRoot<RoleId, Guid>
{
    public string Name { get; private set; }

    private Role(
        string name)
        : base(RoleId.CreateUnique())
    {
        Name = name;
    }

    public static Role Create(
        string name)
    {
        return new Role(
            name);
    }

#pragma warning disable CS8618
    private Role()
    {
    }
#pragma warning restore CS8618
}