﻿using ErrorOr;

using Patron.Domain.Common.Errors;
using Patron.Domain.Common.Models.Identities;

namespace Patron.Domain.RoleAggregate.ValueObjects;

public sealed class RoleId : AggregateRootId<Guid>
{
    private RoleId(Guid value) : base(value)
    {
    }

    public static RoleId Create(Guid userId) =>
        new(userId);

    public static RoleId CreateUnique() =>
        new(Guid.NewGuid());

    public static ErrorOr<RoleId> Create(string value)
    {
        if (!Guid.TryParse(value, out var guid))
        {
            return Errors.Role.InvalidRoleId;
        }

        return new RoleId(guid);
    }
}