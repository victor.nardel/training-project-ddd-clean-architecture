﻿using Patron.Domain.Common.Models;
using Patron.Domain.ReservationAggregate.Events;
using Patron.Domain.ReservationAggregate.ValueObjects;
using Patron.Domain.RoomAggregate.ValueObjects;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Domain.ReservationAggregate;

public sealed class Reservation : AggregateRoot<ReservationId, Guid>
{
    public DateTime DateD { get; private set; }
    public DateTime DateF { get; private set; }
    public float Amount { get; private set; }
    public bool IsPaid { get; private set; }
    public UserId UserId { get; private set; }
    public RoomId RoomId { get; private set; }
    public int PeopleNumber { get; private set; }

    public DateTime CreatedAt { get; private set; }
    public DateTime UpdatedAt { get; private set; }

    private Reservation(
        ReservationId reservationId,
        DateTime dateD,
        DateTime dateF,
        float amount,
        bool isPaid,
        UserId userId,
        RoomId roomId,
        int peopleNumber,
        DateTime? createdAt = null)
        : base(reservationId)
    {
        DateD = dateD;
        DateF = dateF;
        Amount = amount;
        IsPaid = isPaid;
        UserId = userId;
        RoomId = roomId;
        PeopleNumber = peopleNumber;
        CreatedAt = createdAt ?? CreatedAt;
        UpdatedAt = DateTime.Now;
    }

    public static Reservation Create(
        DateTime dateD,
        DateTime dateF,
        float amount,
        bool isPaid,
        UserId userId,
        RoomId roomId,
        int peopleNumber)
    {
        var reservation = new Reservation(
            ReservationId.Create(Guid.NewGuid()),
            dateD,
            dateF,
            amount,
            isPaid,
            userId,
            roomId,
            peopleNumber,
            DateTime.Now);

        reservation.AddDomainEvent(new ReservationCreated(reservation));

        return reservation;
    }

#pragma warning disable CS8618
    private Reservation()
    {
    }
#pragma warning restore CS8618
}