﻿using Patron.Domain.Common.Models.Identities;

namespace Patron.Domain.ReservationAggregate.ValueObjects;

public sealed class ReservationId : AggregateRootId<Guid>
{
    private ReservationId(Guid value) : base(value)
    {
    }

    public static ReservationId Create(Guid roomId) =>
        new(roomId);

    public static ReservationId CreateUnique() =>
        new(Guid.NewGuid());
}