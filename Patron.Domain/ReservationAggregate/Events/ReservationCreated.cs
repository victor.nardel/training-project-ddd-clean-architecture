﻿using Patron.Domain.Common.Models;

namespace Patron.Domain.ReservationAggregate.Events;

public record ReservationCreated(Reservation Reservation) : IDomainEvent;