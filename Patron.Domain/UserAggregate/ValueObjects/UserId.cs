﻿using ErrorOr;

using Patron.Domain.Common.Errors;
using Patron.Domain.Common.Models.Identities;

namespace Patron.Domain.UserAggregate.ValueObjects;

public sealed class UserId : AggregateRootId<Guid>
{
    private UserId(Guid value) : base(value)
    {
    }

    public static UserId Create(Guid userId) =>
        new(userId);

    public static UserId CreateUnique() =>
        new(Guid.NewGuid());

    public static ErrorOr<UserId> Create(string value)
    {
        if (!Guid.TryParse(value, out var guid))
        {
            return Errors.User.InvalidUserId;
        }

        return new UserId(guid);
    }
}