﻿using Patron.Domain.Common.Models;
using Patron.Domain.RoleAggregate;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Domain.UserAggregate;

public sealed class User : AggregateRoot<UserId, Guid>
{
    private readonly List<Role> _roles = [];

    public string Email { get; private set; }
    public string Password { get; private set; }
    public string FirstName { get; private set; }
    public string LastName { get; private set; }
    public DateTime CreatedAt { get; private set; }
    public DateTime UpdatedAt { get; private set; }
    public IReadOnlyList<Role> Roles => _roles.AsReadOnly();

    private User(
        UserId userId,
        string email,
        string password,
        string firstName,
        string lastName,
        List<Role> roles,
        DateTime? createdAt = null)
        : base(userId)
    {
        Email = email;
        Password = password;
        FirstName = firstName;
        LastName = lastName;
        _roles = roles;
        CreatedAt = createdAt ?? CreatedAt;
        UpdatedAt = DateTime.Now;
    }

    public static User Create(
        string email,
        string password,
        string firstName,
        string lastName,
        List<Role>? roles = null)
    {
        return new User(
            UserId.CreateUnique(),
            email,
            password,
            firstName,
            lastName,
            roles ?? [],
            DateTime.Now);
    }

    public static User Update(
        UserId userId,
        string email,
        string password,
        string firstName,
        string lastName,
        List<Role>? roles = null)
    {
        return new User(
            userId,
            email,
            password,
            firstName,
            lastName,
            roles ?? []);
    }

#pragma warning disable CS8618
    private User()
    {
    }
#pragma warning restore CS8618
}