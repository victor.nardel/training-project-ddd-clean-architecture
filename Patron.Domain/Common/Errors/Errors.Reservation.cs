﻿using ErrorOr;

namespace Patron.Domain.Common.Errors;
public static partial class Errors
{
    public static class Reservation
    {
        public static Error ClientIdNotExist => Error.NotFound(
            code: "Reservation.ClientIdNotExist",
            description: "ClientId is not found");

        public static Error RoomIdNotExist => Error.NotFound(
            code: "Reservation.RoomIdNotExist",
            description: "RoomId is not found");

        public static Error ReservationNotExist => Error.NotFound(
            code: "Reservation.ReservationNotExist",
            description: "Reservation is not found");

        public static Error GuidRoomIdBadFormat => Error.Validation(
            code: "Reservation.ReservationNotExist",
            description: "RoomId format is invalid");

        public static Error MaxPeopleExceed => Error.Validation(
            code: "Reservation.MaxPeopleExceed",
            description: "PeopleNumber exceed the max for this room");
    }
}