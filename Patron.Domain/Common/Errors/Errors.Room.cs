﻿using ErrorOr;

namespace Patron.Domain.Common.Errors;

public static partial class Errors
{
    public static class Room
    {
        public static Error DateDIsInvalid => Error.Validation(
            code: "Room.DateDIsInvalid",
            description: "DateD has bad format)");

        public static Error DateFIsInvalid => Error.Validation(
            code: "Room.DateFIsInvalid",
            description: "DateF has bad format");
    }
}