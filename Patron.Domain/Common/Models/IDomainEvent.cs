﻿using MediatR;

namespace Patron.Domain.Common.Models;

public interface IDomainEvent : INotification
{
}