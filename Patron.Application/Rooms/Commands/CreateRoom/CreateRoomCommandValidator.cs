﻿using FluentValidation;

namespace Patron.Application.Rooms.Commands.CreateRoom;

public class CreateRoomCommandValidator : AbstractValidator<CreateRoomCommand>
{
    public CreateRoomCommandValidator()
    {
        RuleFor(x => x.MaxPeople).GreaterThan(0);
        RuleFor(x => x.Type).NotEmpty();
        RuleFor(x => x.Price).GreaterThan(-1);
        RuleFor(x => x.CleanedDate).NotNull();
        RuleFor(x => x.IsCleaned).NotNull();
        RuleFor(x => x.State).NotEmpty();
        RuleFor(x => x.State).NotEmpty();
    }
}