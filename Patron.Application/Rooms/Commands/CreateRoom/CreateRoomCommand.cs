﻿using ErrorOr;

using MediatR;

using Patron.Domain.RoomAggregate;

namespace Patron.Application.Rooms.Commands.CreateRoom;

public record CreateRoomCommand(
    int MaxPeople,
    string Type,
    float Price,
    DateTime CleanedDate,
    bool IsCleaned,
    string State) : IRequest<ErrorOr<Room>>;