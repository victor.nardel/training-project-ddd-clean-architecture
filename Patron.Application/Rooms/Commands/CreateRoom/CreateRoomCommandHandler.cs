﻿using ErrorOr;

using MediatR;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.RoomAggregate;

namespace Patron.Application.Rooms.Commands.CreateRoom;

public class CreateRoomCommandHandler(IRoomRepository roomRepository)
    : IRequestHandler<CreateRoomCommand, ErrorOr<Room>>
{
    public async Task<ErrorOr<Room>> Handle(CreateRoomCommand command, CancellationToken cancellationToken)
    {
        var room = Room.Create(
            maxPeople: command.MaxPeople,
            type: command.Type,
            price: command.Price,
            cleanedDate: command.CleanedDate,
            isCleaned: command.IsCleaned,
            state: command.State);

        await roomRepository.AddAsync(room);

        return room;
    }
}