﻿using ErrorOr;

using MediatR;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.RoomAggregate;

namespace Patron.Application.Rooms.Queries.AllRooms;

public class AllRoomsQueryHandler(IRoomRepository roomRepository)
    : IRequestHandler<AllRoomsQuery, ErrorOr<List<Room>>>
{
    public async Task<ErrorOr<List<Room>>> Handle(
        AllRoomsQuery query,
        CancellationToken cancellationToken) =>
            await roomRepository.GetAllAsync();
}