﻿using ErrorOr;

using MediatR;

using Patron.Domain.RoomAggregate;

namespace Patron.Application.Rooms.Queries.AllRooms;

public record AllRoomsQuery() : IRequest<ErrorOr<List<Room>>>;