﻿using ErrorOr;

using MediatR;

using Patron.Domain.RoleAggregate;
using Patron.Domain.UserAggregate;

namespace Patron.Application.Users.Commands.UpdateUser;

public record UpdateUserCommand(
    string UserId,
    string Email,
    string Password,
    string FirstName,
    string LastName,
    List<Role> Roles) : IRequest<ErrorOr<User>>;