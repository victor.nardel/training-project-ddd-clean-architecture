﻿using ErrorOr;

using MediatR;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.Common.Errors;
using Patron.Domain.UserAggregate;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Application.Users.Commands.UpdateUser;

public class UpdateUserCommandHandler(IUserRepository userRepository)
    : IRequestHandler<UpdateUserCommand, ErrorOr<User>>
{
    public async Task<ErrorOr<User>> Handle(UpdateUserCommand command, CancellationToken cancellationToken)
    {
        var createUserIdResult = UserId.Create(command.UserId);

        if (createUserIdResult.IsError)
        {
            return createUserIdResult.Errors;
        }

        if (await userRepository.GetAsync(createUserIdResult.Value) is null)
        {
            return Errors.User.UserNotFound;
        }

        var userUpdated = User.Update(
            userId: createUserIdResult.Value,
            email: command.Email,
            password: command.Password,
            firstName: command.FirstName,
            lastName: command.LastName);

        await userRepository.UpdateAsync(userUpdated);

        return userUpdated;
    }
}