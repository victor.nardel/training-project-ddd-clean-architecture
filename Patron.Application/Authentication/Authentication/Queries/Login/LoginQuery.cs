﻿using ErrorOr;

using MediatR;

using Patron.Application.Authentication.Authentication.Common;

namespace Patron.Application.Authentication.Authentication.Queries.Login;

public record LoginQuery(
    string Email,
    string Password) : IRequest<ErrorOr<AuthenticationResult>>;