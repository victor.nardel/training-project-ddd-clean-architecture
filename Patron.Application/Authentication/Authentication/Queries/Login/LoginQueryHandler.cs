﻿using ErrorOr;

using MediatR;

using Patron.Application.Authentication.Authentication.Common;
using Patron.Application.Common.Interfaces.Authentication;
using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.Common.Errors;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Application.Authentication.Authentication.Queries.Login;

public class LoginQueryHandler(
    IUserRepository userRepository,
    IJwtTokenGenerator jwtTokenGenerator,
    IPasswordHasher passwordHasher)
        : IRequestHandler<LoginQuery, ErrorOr<AuthenticationResult>>
{
    public async Task<ErrorOr<AuthenticationResult>> Handle(
        LoginQuery query,
        CancellationToken cancellationToken)
    {
        if (await userRepository.GetByEmail(query.Email) is not { } user)
        {
            return Errors.Authentication.InvalidCredentials;
        }

        if (!passwordHasher.VerifyPassword(query.Password, user.Password))
        {
            return Errors.Authentication.InvalidCredentials;
        }

        return new AuthenticationResult(
            user,
            jwtTokenGenerator.GenerateToken(user, user.Roles.ToList()));
    }
}