﻿using Patron.Domain.UserAggregate;

namespace Patron.Application.Authentication.Authentication.Common;

public record AuthenticationResult(
    User User,
    string Token);