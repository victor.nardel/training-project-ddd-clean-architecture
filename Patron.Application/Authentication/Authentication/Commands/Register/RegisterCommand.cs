﻿using ErrorOr;

using MediatR;

using Patron.Application.Authentication.Authentication.Common;

namespace Patron.Application.Authentication.Authentication.Commands.Register;

public record RegisterCommand(
    string Email,
    string Password,
    string FirstName,
    string LastName,
    List<string> RoleIds) : IRequest<ErrorOr<AuthenticationResult>>;