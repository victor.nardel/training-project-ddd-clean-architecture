﻿using ErrorOr;

using MediatR;

using Patron.Application.Authentication.Authentication.Common;
using Patron.Application.Common.Interfaces.Authentication;
using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.Common.Errors;
using Patron.Domain.RoleAggregate.ValueObjects;
using Patron.Domain.UserAggregate;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Application.Authentication.Authentication.Commands.Register;

public class RegisterCommandHandler(
    IUserRepository userRepository,
    IRoleRepository roleRepository,
    IJwtTokenGenerator jwtTokenGenerator,
    IPasswordHasher passwordHasher)
    : IRequestHandler<RegisterCommand, ErrorOr<AuthenticationResult>>
{
    public async Task<ErrorOr<AuthenticationResult>> Handle(
        RegisterCommand command,
        CancellationToken cancellationToken)
    {
        if (await userRepository.GetByEmail(command.Email) is not null)
        {
            return Errors.User.DuplicateEmail;
        }

        var roleIds = command.RoleIds.Select(r => RoleId.Create(r).Value).ToList();

        var roles = await roleRepository.GetRolesByIdsAsync(roleIds);

        var user = User.Create(
            command.Email,
            passwordHasher.HashPassword(command.Password),
            command.FirstName,
            command.LastName,
            roles);

        await userRepository.AddAsync(user);

        return new AuthenticationResult(
            user,
            jwtTokenGenerator.GenerateToken(user, roles));
    }
}