﻿using Patron.Application.Common.Interfaces.Persistence.Common;
using Patron.Domain.RoleAggregate;
using Patron.Domain.RoleAggregate.ValueObjects;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Application.Common.Interfaces.Persistence;

public interface IRoleRepository : IRepository<Role, RoleId>
{
    Task<List<Role>> GetRolesByIdsAsync(List<RoleId> roleIds);
}