﻿using Patron.Application.Common.Interfaces.Persistence.Common;
using Patron.Domain.UserAggregate;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Application.Common.Interfaces.Persistence;

public interface IUserRepository : IRepository<User, UserId>
{
    Task<User?> GetByEmail(string email);
}