﻿using Patron.Application.Common.Interfaces.Persistence.Common;
using Patron.Domain.ReservationAggregate;
using Patron.Domain.ReservationAggregate.ValueObjects;
using Patron.Domain.RoomAggregate.ValueObjects;

namespace Patron.Application.Common.Interfaces.Persistence;

public interface IReservationRepository : IRepository<Reservation, ReservationId>
{
    Task<List<RoomId>> GetAvailableRoomIds(DateTime dateD, DateTime dateF);
}