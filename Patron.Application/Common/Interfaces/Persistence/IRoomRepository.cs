﻿using Patron.Application.Common.Interfaces.Persistence.Common;
using Patron.Domain.RoomAggregate;
using Patron.Domain.RoomAggregate.ValueObjects;

namespace Patron.Application.Common.Interfaces.Persistence;

public interface IRoomRepository : IRepository<Room, RoomId>
{
    Task<List<Room>> GetRoomListById(List<RoomId> roomIds);
}