﻿using Patron.Domain.RoleAggregate;
using Patron.Domain.UserAggregate;

namespace Patron.Application.Common.Interfaces.Authentication;
public interface IJwtTokenGenerator
{
    string GenerateToken(User user, List<Role>? roles = null);
}