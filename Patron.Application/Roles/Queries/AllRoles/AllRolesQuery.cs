﻿using ErrorOr;

using MediatR;

using Patron.Domain.RoleAggregate;

namespace Patron.Application.Roles.Queries.AllRoles;

public record AllRolesQuery() : IRequest<ErrorOr<List<Role>>>;