﻿using ErrorOr;

using MediatR;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.RoleAggregate;

namespace Patron.Application.Roles.Queries.AllRoles;

public class AllRolesQueryhandler(IRoleRepository roleRepository)
    : IRequestHandler<AllRolesQuery, ErrorOr<List<Role>>>
{
    public async Task<ErrorOr<List<Role>>> Handle(
        AllRolesQuery query,
        CancellationToken cancellationToken)
    {
        try
        {
            return await roleRepository.GetAllAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}