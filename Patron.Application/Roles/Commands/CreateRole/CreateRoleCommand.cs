﻿using ErrorOr;

using MediatR;

using Patron.Domain.RoleAggregate;

namespace Patron.Application.Roles.Commands.CreateRole;

public record CreateRoleCommand(
    string Name) : IRequest<ErrorOr<Role>>;