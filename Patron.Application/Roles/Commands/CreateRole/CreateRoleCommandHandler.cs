﻿using ErrorOr;

using MediatR;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.RoleAggregate;

namespace Patron.Application.Roles.Commands.CreateRole;

public class CreateRoleCommandHandler(IRoleRepository roleRepository)
    : IRequestHandler<CreateRoleCommand, ErrorOr<Role>>
{
    public async Task<ErrorOr<Role>> Handle(
        CreateRoleCommand command,
        CancellationToken cancellationToken)
    {
        var room = Role.Create(
            name: command.Name);

        await roleRepository.AddAsync(room);

        return room;
    }
}