﻿using FluentValidation;

namespace Patron.Application.Reservations.Commands.CreateReservation;

public class CreateReservationValidator : AbstractValidator<CreateReservationCommand>
{
    public CreateReservationValidator()
    {
        RuleFor(x => x.DateD).NotNull().GreaterThanOrEqualTo(DateTime.Now.Date);
        RuleFor(x => x.DateF).NotNull();
        RuleFor(x => x.Amount).GreaterThan(-1);
        RuleFor(x => x.UserId).NotEmpty();
        RuleFor(x => x.RoomId).NotEmpty();
        RuleFor(x => x.PeopleNumber).NotNull().GreaterThanOrEqualTo(0);
    }
}