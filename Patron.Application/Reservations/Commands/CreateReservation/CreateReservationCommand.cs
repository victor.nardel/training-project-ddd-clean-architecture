﻿using ErrorOr;

using MediatR;

using Patron.Domain.ReservationAggregate;

namespace Patron.Application.Reservations.Commands.CreateReservation;

public record CreateReservationCommand(
    DateTime DateD,
    DateTime DateF,
    float Amount,
    bool IsPaid,
    string UserId,
    string RoomId,
    int PeopleNumber) : IRequest<ErrorOr<Reservation>>;