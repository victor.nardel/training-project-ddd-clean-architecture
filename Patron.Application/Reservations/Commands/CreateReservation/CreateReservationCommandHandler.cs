﻿using ErrorOr;

using MediatR;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.Common.Errors;
using Patron.Domain.ReservationAggregate;
using Patron.Domain.RoomAggregate.ValueObjects;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Application.Reservations.Commands.CreateReservation;

public class CreateReservationCommandHandler(
    IRoomRepository roomRepository,
    IUserRepository userRepository,
    IReservationRepository reservationRepository)
        : IRequestHandler<CreateReservationCommand, ErrorOr<Reservation>>
{
    public async Task<ErrorOr<Reservation>> Handle(
        CreateReservationCommand command,
        CancellationToken cancellationToken)
    {
        if (await userRepository.GetAsync(UserId.Create(command.UserId).Value) is not { } user)
        {
            return Errors.Reservation.ClientIdNotExist;
        }

        if (await roomRepository.GetAsync(RoomId.Create(new Guid(command.RoomId))) is not { } room)
        {
            return Errors.Reservation.RoomIdNotExist;
        }

        if (command.PeopleNumber > room.MaxPeople)
        {
            return Errors.Reservation.MaxPeopleExceed;
        }

        var reservation = Reservation.Create(
            dateD: command.DateD,
            dateF: command.DateF,
            amount: command.Amount,
            isPaid: command.IsPaid,
            userId: (UserId)user.Id,
            roomId: (RoomId)room.Id,
            peopleNumber: command.PeopleNumber);

        await reservationRepository.AddAsync(reservation);

        return reservation;
    }
}