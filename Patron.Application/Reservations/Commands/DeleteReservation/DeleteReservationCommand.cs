﻿using ErrorOr;

using MediatR;

using Patron.Domain.ReservationAggregate;

namespace Patron.Application.Reservations.Commands.DeleteReservation;

public record DeleteReservationCommand(
    string Id) : IRequest<ErrorOr<Reservation>>;