﻿using ErrorOr;

using MediatR;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.Common.Errors;
using Patron.Domain.ReservationAggregate;
using Patron.Domain.ReservationAggregate.ValueObjects;

namespace Patron.Application.Reservations.Commands.DeleteReservation;

public class DeleteReservationCommandHandler(
    IReservationRepository reservationRepository)
        : IRequestHandler<DeleteReservationCommand, ErrorOr<Reservation>>
{
    public async Task<ErrorOr<Reservation>> Handle(
        DeleteReservationCommand command,
        CancellationToken cancellationToken)
    {
        if (!Guid.TryParse(command.Id, out Guid guid))
        {
            return Errors.Reservation.GuidRoomIdBadFormat;
        }

        var reservationId = ReservationId.Create(guid);

        if (await reservationRepository.GetAsync(reservationId) is not { } reservation)
        {
            return Errors.Reservation.ReservationNotExist;
        }

        await reservationRepository.DeleteAsync(reservation);

        return reservation;
    }
}