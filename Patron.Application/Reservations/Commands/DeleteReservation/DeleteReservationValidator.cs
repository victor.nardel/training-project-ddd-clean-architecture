﻿using FluentValidation;

namespace Patron.Application.Reservations.Commands.DeleteReservation;

public class DeleteReservationValidator : AbstractValidator<CreateReservation.CreateReservationCommand>
{
    public DeleteReservationValidator()
    {
        RuleFor(x => x.DateD).NotNull().GreaterThanOrEqualTo(DateTime.Now.Date);
        RuleFor(x => x.DateF).NotNull();
        RuleFor(x => x.Amount).GreaterThan(-1);
        RuleFor(x => x.UserId).NotEmpty();
        RuleFor(x => x.RoomId).NotEmpty();
    }
}