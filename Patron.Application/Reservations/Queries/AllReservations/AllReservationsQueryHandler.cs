﻿using ErrorOr;

using MediatR;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.ReservationAggregate;

namespace Patron.Application.Reservations.Queries.AllReservations;

public class AllReservationsQueryHandler(IReservationRepository reservationRepository)
    : IRequestHandler<AllReservationsQuery, ErrorOr<List<Reservation>>>
{
    public async Task<ErrorOr<List<Reservation>>> Handle(
        AllReservationsQuery query,
        CancellationToken cancellationToken) =>
            await reservationRepository.GetAllAsync();
}