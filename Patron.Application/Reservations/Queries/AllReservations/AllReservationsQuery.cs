﻿using ErrorOr;

using MediatR;

using Patron.Domain.ReservationAggregate;

namespace Patron.Application.Reservations.Queries.AllReservations;

public record AllReservationsQuery() : IRequest<ErrorOr<List<Reservation>>>;