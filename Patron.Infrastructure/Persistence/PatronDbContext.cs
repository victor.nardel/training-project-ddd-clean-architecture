﻿using Microsoft.EntityFrameworkCore;

using Patron.Domain.Common.Models;
using Patron.Domain.ReservationAggregate;
using Patron.Domain.RoleAggregate;
using Patron.Domain.RoomAggregate;
using Patron.Domain.UserAggregate;
using Patron.Infrastructure.Persistence.Interceptors;

namespace Patron.Infrastructure.Persistence;

public class PatronDbContext(
    DbContextOptions<PatronDbContext> options,
    PublishDomainEventsInterceptor publishDomainEventsInterceptor)
    : DbContext(options)
{
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Role> Roles { get; set; } = null!;
    public DbSet<Reservation> Reservations { get; set; } = null!;
    public DbSet<Room> Rooms { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Ignore<List<IDomainEvent>>()
            .ApplyConfigurationsFromAssembly(typeof(PatronDbContext).Assembly);

        base.OnModelCreating(modelBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.AddInterceptors(publishDomainEventsInterceptor);
        base.OnConfiguring(optionsBuilder);
    }
}