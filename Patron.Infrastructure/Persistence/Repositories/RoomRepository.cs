﻿using Microsoft.EntityFrameworkCore;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.RoomAggregate;
using Patron.Domain.RoomAggregate.ValueObjects;

namespace Patron.Infrastructure.Persistence.Repositories;

public class RoomRepository(PatronDbContext dbContext) : IRoomRepository
{
    public async Task<List<Room>> GetAllAsync() =>
        await dbContext.Rooms.ToListAsync();

    public async Task<Room?> GetAsync(RoomId id) =>
        await dbContext.Rooms.FirstOrDefaultAsync(r => r.Id == id);

    public async Task AddAsync(Room entity)
    {
        dbContext.Add(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(Room entity)
    {
        dbContext.Rooms.Update(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(Room entity)
    {
        dbContext.Rooms.Remove(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task<List<Room>> GetRoomListById(List<RoomId> roomIds)
    {
        var rooms = new List<Room>();
        foreach (var room in await dbContext.Rooms.ToListAsync())
        {
            foreach (var roomId in roomIds)
            {
                if (room.Id.Equals(roomId))
                {
                    rooms.Add(room);
                }
            }
        }

        return rooms;
    }
}