﻿using Microsoft.EntityFrameworkCore;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.ReservationAggregate;
using Patron.Domain.ReservationAggregate.ValueObjects;
using Patron.Domain.RoomAggregate.ValueObjects;

namespace Patron.Infrastructure.Persistence.Repositories;

public class ReservationRepository(PatronDbContext dbContext) : IReservationRepository
{
    public async Task<List<Reservation>> GetAllAsync() =>
        await dbContext.Reservations.ToListAsync();

    public async Task<Reservation?> GetAsync(ReservationId id) =>
        await dbContext.Reservations.FirstOrDefaultAsync(r => r.Id == id);

    public async Task AddAsync(Reservation entity)
    {
        dbContext.Add(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(Reservation entity)
    {
        dbContext.Reservations.Update(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(Reservation entity)
    {
        dbContext.Reservations.Remove(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task<List<RoomId>> GetAvailableRoomIds(DateTime dateD, DateTime dateF) =>
        await dbContext.Reservations
            .Where(r => r.DateD > dateF || r.DateF < dateD)
            .Select(r => r.RoomId)
            .ToListAsync();
}