﻿using System.Data;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.RoleAggregate;
using Patron.Domain.RoleAggregate.ValueObjects;

namespace Patron.Infrastructure.Persistence.Repositories;

public class RoleRepository(PatronDbContext dbContext) : IRoleRepository
{
    public async Task<List<Role>> GetAllAsync() =>
        await dbContext.Roles.ToListAsync();

    public async Task<Role?> GetAsync(RoleId id) =>
        await dbContext.Roles.FirstOrDefaultAsync(r => r.Id == id);

    public async Task AddAsync(Role entity)
    {
        dbContext.Add(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(Role entity)
    {
        dbContext.Roles.Update(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(Role entity)
    {
        dbContext.Roles.Remove(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task<List<Role>> GetRolesByIdsAsync(List<RoleId> roleIds)
    {
        var parameter = new SqlParameter("roleIds", SqlDbType.VarChar)
        {
            Value = string.Join(",", roleIds.Select(id => id.ToString())),
        };

        const string sql = "SELECT * FROM Roles WHERE Id IN (SELECT value FROM STRING_SPLIT(@roleIds, ','))";
        return await dbContext.Roles.FromSqlRaw(sql, parameter).ToListAsync();
    }
}