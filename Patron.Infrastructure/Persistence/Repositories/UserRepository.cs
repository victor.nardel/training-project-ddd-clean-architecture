﻿using Microsoft.EntityFrameworkCore;

using Patron.Application.Common.Interfaces.Persistence;
using Patron.Domain.UserAggregate;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Infrastructure.Persistence.Repositories;

public class UserRepository(PatronDbContext dbContext) : IUserRepository
{
    public async Task<List<User>> GetAllAsync() =>
        await dbContext.Users.ToListAsync();

    public async Task<User?> GetAsync(UserId id) =>
        await dbContext.Users
            .Include(u => u.Roles)
            .FirstOrDefaultAsync(u => u.Id == id);

    public async Task AddAsync(User entity)
    {
        dbContext.Add(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(User entity)
    {
        dbContext.Users.Update(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(User entity)
    {
        dbContext.Users.Remove(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task<User?> GetByEmail(string email) =>
        await dbContext.Users
            .Include(u => u.Roles)
            .FirstOrDefaultAsync(u => u.Email == email);
}