﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Patron.Domain.RoomAggregate;
using Patron.Domain.RoomAggregate.ValueObjects;

namespace Patron.Infrastructure.Persistence.Configurations;

public class RoomConfigurations : IEntityTypeConfiguration<Room>
{
    public void Configure(EntityTypeBuilder<Room> builder)
    {
        ConfigureRoomTable(builder);
    }

    private static void ConfigureRoomTable(EntityTypeBuilder<Room> builder)
    {
        builder.ToTable("Rooms");

        builder.HasKey(r => r.Id);
        builder.Property(r => r.Id)
            .ValueGeneratedNever()
            .HasConversion(
                id => id.Value,
                value => RoomId.Create(value));

        builder.Property(u => u.MaxPeople);
        builder.Property(u => u.Type).HasMaxLength(100);
        builder.Property(u => u.Price);
        builder.Property(u => u.CleanedDate);
        builder.Property(u => u.IsCleaned);
        builder.Property(u => u.State).HasMaxLength(100);
    }
}