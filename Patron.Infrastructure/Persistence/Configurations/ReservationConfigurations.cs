﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Patron.Domain.ReservationAggregate;
using Patron.Domain.ReservationAggregate.ValueObjects;
using Patron.Domain.RoomAggregate.ValueObjects;
using Patron.Domain.UserAggregate.ValueObjects;

namespace Patron.Infrastructure.Persistence.Configurations;

public class ReservationConfigurations : IEntityTypeConfiguration<Reservation>
{
    public void Configure(EntityTypeBuilder<Reservation> builder)
    {
        ConfigureReservationTable(builder);
    }

    private static void ConfigureReservationTable(EntityTypeBuilder<Reservation> builder)
    {
        builder.ToTable("Reservations");

        builder.HasKey(r => r.Id);
        builder.Property(r => r.Id)
            .ValueGeneratedNever()
            .HasConversion(
                id => id.Value,
                value => ReservationId.Create(value));

        builder.Property(r => r.DateD);
        builder.Property(r => r.DateF);
        builder.Property(r => r.Amount);
        builder.Property(r => r.IsPaid);
        builder.Property(r => r.PeopleNumber);

        builder.Property(r => r.UserId)
            .HasConversion(
                id => id.Value,
                value => UserId.Create(value));

        builder.Property(r => r.RoomId)
            .HasConversion(
                id => id.Value,
                value => RoomId.Create(value));
    }
}