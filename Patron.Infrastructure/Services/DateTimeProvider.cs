﻿using Patron.Application.Common.Interfaces.Services;

namespace Patron.Infrastructure.Services;

public class DateTimeProvider : IDateTimeProvider
{
    public DateTime UtcNow => DateTime.UtcNow;
}