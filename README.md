<a name="readme-top"></a>

## Getting Started

### Prerequisites

You must have Dotnet and dotnet-ef installed.
* [https://dotnet.microsoft.com/en-us/download](https://dotnet.microsoft.com/en-us/download)
   ```sh
   dotnet tool install --global dotnet-ef
   ```

### Installation

1. Clone the repository
   ```sh
   git clone https://gitlab.com/victor.nardel/architecture_morin_nardella.git
   ```

2. Install all dependencies
   ```sh
   dotnet restore
   ```

3. Migrate to database
Go to ".\Patron.Infrastructure\DependencyInjection.cs" and adapt the connection string.
Then, run the command for the migration :
   ```sh
   dotnet ef database update -p ./Patron.Infrastructure -s ./Patron.Api
   ```

4. Run the project
   ```sh
   dotnet run --project .\Patron.Api\ --environment=Development
   ```
   
Then, you can open http://localhost:5091/swagger/ in your browser.